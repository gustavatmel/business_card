"""

Written by Lorenz Roos 
Copyright (c) 2019 0x220E
Licensed under the MIT license.

"""

# pylint: disable=global-statement,stop-iteration-return,no-self-use,useless-super-delegation

import time
import random
import board
import digitalio
import adafruit_dotstar
from adafruit_debouncer import Debouncer

# Implementation dependant things to tweak
NUM_PIXELS = 6               # number of neopixels in the striup
DROP_DURATION = 10.0         # how many seconds the ball takes to drop

# Pins
DOTSTAR_SCK = board.APA102_SCK
DOTSTAR_MOSI = board.APA102_MOSI
SW1_PIN = board.SW1
SW2_PIN = board.SW2
SW3_PIN = board.SW3
SW4_PIN = board.SW4
SW5_PIN = board.SW5
SW6_PIN = board.SW6

SW_IO = [SW1_PIN, SW2_PIN, SW3_PIN, SW4_PIN, SW5_PIN, SW6_PIN]

################################################################################
# Setup hardware

pixels  = adafruit_dotstar.DotStar(board.APA102_SCK, board.APA102_MOSI, NUM_PIXELS, brightness=0.2, auto_write=False)
pixels.fill(0)                          # Dotstar off ASAP on startup
pixels.show()

switch_io = []
switch = []

for x in SW_IO:
    switch_io.append(digitalio.DigitalInOut(x))
    switch_io[-1].direction = digitalio.Direction.INPUT
    switch_io[-1].pull = digitalio.Pull.UP
    switch.append(Debouncer(switch_io[-1]))

################################################################################
# Global Variables

RED = (255, 0, 0)
YELLOW = (255, 150, 0)
GREEN = (0, 255, 0)
CYAN = (0, 255, 255)
BLUE = (0, 0, 255)
PURPLE = (180, 0, 255)

COLOR = [RED, GREEN, CYAN, BLUE, YELLOW, PURPLE]

################################################################################
# Random color

def random_color_byte():
    """ Return one of 32 evenly spaced byte values.
    This provides random colors that are fairly distinctive."""
    return random.randrange(0, 256, 16)

def random_color():
    """Return a random color"""
    red = random_color_byte()
    green = random_color_byte()
    blue = random_color_byte()
    return (red, green, blue)

# Color cycling.

def wheel(pos):
    # Input a value 0 to 255 to get a color value.
    # The colours are a transition r - g - b - back to r.
    if pos < 0 or pos > 255:
        return 0, 0, 0
    if pos < 85:
        return int(255 - pos*3), int(pos*3), 0
    if pos < 170:
        pos -= 85
        return 0, int(255 - pos*3), int(pos*3)
    pos -= 170
    return int(pos * 3), 0, int(255 - (pos*3))

def cycle_sequence(seq):
    while True:
        for elem in seq:
            yield elem

def rainbow_lamp(seq):
    g = cycle_sequence(seq)
    while True:
        pixels.fill(wheel(next(g)))
        pixels.show()
        yield

def simon_code(seq):
    for x in seq:
            pixels[x] = COLOR[x]                         
            pixels.show()
            time.sleep(0.5)
            pixels.fill(0)
            pixels.show()
            time.sleep(0.5)

def switch_state(color, num):
    for x in range(num):
        pixels.fill(color)                         
        pixels.show()
        time.sleep(0.2)
        pixels.fill(0)                         
        pixels.show()
        time.sleep(0.2)
    

################################################################################
# State Machine

class StateMachine(object):

    def __init__(self):
        self.state = None
        self.states = {}

    def add_state(self, state):
        self.states[state.name] = state

    def go_to_state(self, state_name):
        if self.state:
            self.state.exit(self)
        self.state = self.states[state_name]
        self.state.enter(self)

    def update(self):
        if self.state:
            self.state.update(self)

################################################################################
# States


# Abstract parent state class.

class State(object):

    def __init__(self):
        pass

    @property
    def name(self):
        return ''

    def enter(self, machine):
        pass

    def exit(self, machine):
        pass

    def update(self, machine):
        return True


class MainState(State):

    def __init__(self):
        super().__init__()

    @property
    def name(self):
        return 'main'

    def enter(self, machine):
        State.enter(self, machine)
        pixels[0] = RED
        pixels[1] = GREEN
        pixels[2] = CYAN
        pixels[3] = BLUE
        pixels[4] = YELLOW
        pixels[5] = PURPLE
        pixels.show()

    def exit(self, machine):
        State.exit(self, machine)
        pixels.fill(0)                         
        pixels.show()

    def update(self, machine):
        # No super call to check for switch press to pause
        # switch press here drops the ball
        # if switch[0].fell:
        #     machine.go_to_state('rainbow')
        for x in range(6):
            if switch[x].fell:
                machine.go_to_state('simon')


# class RainbowState(State):

#     def __init__(self):
#         super().__init__()
#         self.rainbow = None
#         self.rainbow_time = 0
#         self.drop_finish_time = 0

#     @property
#     def name(self):
#         return 'rainbow'

#     def enter(self, machine):
#         State.enter(self, machine)
#         now = time.monotonic()
#         self.rainbow = rainbow_lamp(range(0, 256, 2))
#         self.rainbow_time = now + 0.1
#         self.drop_finish_time = now + DROP_DURATION


#     def exit(self, machine):
#         State.exit(self, machine)
#         pixels.fill(0)                         
#         pixels.show()

#     def update(self, machine):
#         if switch[0].fell:
#             machine.go_to_state('main')
#         if State.update(self, machine):
#             now = time.monotonic()
#             if now >= self.rainbow_time:
#                 next(self.rainbow)
#                 self.rainbow_time = now + 0.1

class SimonState(State):

    def __init__(self):
        super().__init__()
        self.simon_seq = []
        self.user_seq = []
        self.disp_seq = False


    @property
    def name(self):
        return 'simon'

    def enter(self, machine):
        State.enter(self, machine)
        for x in range(4):
            self.simon_seq.append(random.randrange(6))
        self.disp_seq = True
        switch_state(GREEN, 2)
        time.sleep(0.5)


    def exit(self, machine):
        State.exit(self, machine)
        pixels.fill(0)                         
        pixels.show()
        self.simon_seq = []
        self.user_seq = []
        self.disp_seq = False

    def update(self, machine):          
        if self.disp_seq:
            simon_code(self.simon_seq)
            self.disp_seq = False

        for x in range(6):
            if switch[x].fell:
                self.user_seq.append(x)
                pixels[x] = COLOR[x]
                pixels.show()

            if switch[x].rose:
                pixels[x] = (0,0,0)
                pixels.show()


        if len(self.user_seq):
            for x in range(len(self.user_seq)):
                diff = self.user_seq[x] - self.simon_seq[x]

            if not diff and len(self.user_seq) == len(self.simon_seq):
                switch_state(GREEN, 2)
                time.sleep(0.5)
                self.simon_seq.append(random.randrange(6))
                self.disp_seq = True
                self.user_seq = []


            if diff:
                switch_state(RED, 2)
                machine.go_to_state('main')
                time.sleep(0.5)


        print(self.user_seq)

        

################################################################################
# Create the state machine

pretty_state_machine = StateMachine()
pretty_state_machine.add_state(MainState())
#pretty_state_machine.add_state(RainbowState())
pretty_state_machine.add_state(SimonState())

pretty_state_machine.go_to_state('main')

while True:
    for x in range(6):
        switch[x].update()



    pretty_state_machine.update()
